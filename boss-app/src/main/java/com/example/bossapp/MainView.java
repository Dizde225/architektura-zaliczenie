package com.example.bossapp;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

@Route("")
public class MainView extends VerticalLayout {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    private Label labe_addTask = new Label("Add new task");
    private TextField textField_name = new TextField("Name");
    private TextField textField_description = new TextField("Description");
    private Button button_send = new Button("Send Task");
    private HorizontalLayout horizontalLayout = new HorizontalLayout(textField_name, textField_description);

    public MainView(){
        add(labe_addTask);
        add(horizontalLayout);
        add(button_send);
        textField_name.focus();
        button_send.addClickShortcut(Key.ENTER);
        button_send.addClickListener(buttonClickEvent -> doIt());
    }

    private void doIt() {
        Task task = new Task(textField_name.getValue(), textField_description.getValue());
        rabbitTemplate.convertAndSend("tasks",task.toString());
        clearFields();
    }

    private void clearFields(){
        textField_name.setValue("");
        textField_description.setValue("");
        textField_name.focus();
    }

}
