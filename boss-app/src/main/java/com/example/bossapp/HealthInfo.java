package com.example.bossapp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthInfo {
    @GetMapping("/alive")
    public String healthCheck(){
        return "it's alive!";
    }
}
