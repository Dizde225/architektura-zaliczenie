package com.example.clientapp;

import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
@Component
public class QueueHealth extends AbstractHealthIndicator {
    @Override
    protected void doHealthCheck(Health.Builder builder) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        try {
            ResponseEntity<String> exchange = restTemplate.exchange("http://localhost:8080/alive", HttpMethod.GET, HttpEntity.EMPTY, String.class);
            builder.up().withDetail("Connection status:","boss app is alive!");
        }catch (Exception e){
            builder.down().withDetail("Connection status:","boss app is dead");
        }
    }

}
