package com.example.clientapp;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;


@Route("")
public class MainView extends VerticalLayout {

    @Autowired
    private TaskApi taskApi;

    @Autowired
    private TaskRepository taskRepository;

    private Task task;

    private Button button_showTask = new Button("Next Task");
    private Button button_showPostponedTask = new Button("Postponed Task");
    private Button button_done = new Button("Done");
    private Button button_postpone = new Button("Postpone");
    private Button button_showAllDone = new Button("Show all done");

    private TextArea textArea_name = new TextArea("Name");
    private TextArea textArea_description = new TextArea("Description");
    private TextArea textArea_allTask = new TextArea("All Tasks");

    private HorizontalLayout horizontalLayout_buttons = new HorizontalLayout(
            button_showTask,
            button_showPostponedTask,
            button_postpone,
            button_done,
            button_showAllDone
    );

    private HorizontalLayout horizontalLayout_labels = new HorizontalLayout(
            textArea_name,
            textArea_description
    );

    public MainView() {
        add(horizontalLayout_buttons);
        add(horizontalLayout_labels);
        add(textArea_allTask);

        button_showTask.addClickListener(buttonClickEvent -> onClickShowTask());
        button_showPostponedTask.addClickListener(buttonClickEvent -> onClickShowPostponedTask());
        button_postpone.addClickListener(buttonClickEvent -> onClickPostpone());
        button_done.addClickListener(buttonClickEvent -> onClickDone());
        button_showAllDone.addClickListener(buttonClickEvent -> onClickViewAllDoneTask());

    }

    private void onClickShowTask(){
        clearTextArea();
        if (task != null) {
            textArea_allTask.setValue("Do something with a task!");
            return;
        }
        task = taskApi.getTask(0);
        if (task == null) {
            textArea_allTask.setValue("There is no task");
            return;
        }
        setLabels(task);
    }

    private void onClickShowPostponedTask(){
        clearTextArea();
        if (task != null){
            textArea_allTask.setValue("Do something with a task!");
            return;
        }
        task = taskApi.getTask(1);
        if (task == null) {
            textArea_allTask.setValue("There is no task");
            return;
        }
        setLabels(task);
    }

    private void onClickDone(){
        clearTextArea();
        if (task == null) {
            textArea_allTask.setValue("There is no task");
            return;
        }
        task.setLocalDate(LocalDateTime.now());
        taskRepository.save(task);
        task = null;
        textArea_allTask.setValue("DONE!");
    }

    private void onClickPostpone(){
        clearTextArea();
        if (task == null){
            return;
        }
        String message = taskApi.postponeTask(task);
        task = null;
        textArea_allTask.setValue(message);
    }

    private void onClickViewAllDoneTask(){
        clearTextArea();
        StringBuilder sb = new StringBuilder();
        for (Task t : taskRepository.findAll()) {
            sb.append(t.toString());
            sb.append(" ");
            sb.append(t.getLocalDate().toString());
            sb.append("\n");
        }
        String allTask = sb.toString();
        textArea_allTask.setValue(allTask);
    }

    private void setLabels(Task task) {
        String name = task.getName();
        String description = task.getDescription();
        textArea_name.setValue(name);
        textArea_description.setValue(description);
    }

    private void clearTextArea(){
        textArea_name.clear();
        textArea_description.clear();
        textArea_allTask.clear();
    }
}
