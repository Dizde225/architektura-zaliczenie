package com.example.clientapp;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TaskApi {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public Task getTask(int num){
        String temp = null;
        if(num == 0){
            temp = (String) rabbitTemplate.receiveAndConvert("tasks");
        }
        if (num == 1) {
            temp = (String) rabbitTemplate.receiveAndConvert("postponed");
        }
        if (temp == null) {
            return null;
        }
        String [] split = temp.split("#");
        return new Task(split[0], split[1]);
    }

    public String postponeTask(Task task){
        rabbitTemplate.convertAndSend("postponed", task.toString2());
        return "Task has been postponed!";
    }
}
